<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'yaousti',
            'email' => 'yaousti@gmail.com',
            'password' => bcrypt('P@ssw0rd')
        ]);

        $role = Role::create([
            'name' => 'super_admin',
        ]);

        $user->assignRole([$role->id]);
    }
}
