<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://unpkg.com/tailwindcss@^1.0/dist/tailwind.min.css" rel="stylesheet">
    <title>Applied Job</title>
    <style>
        #msg {
            display: flex;
            justify-content: center;
        }

        .span_p {
            font-weight: bold;
            font-family: Ubuntu;
            color: purple;
        }

        p {
            font-family: Ubuntu;
        }

        img {
            max-width: 130px;
            max-height: 50px;
        }

        .s {
            font-family: Ubuntu;
            font-weight: bold;
        }
    </style>
</head>

<body>
    <div id="msg">
        <p>
            Bonjour <span class="span_p">{{$last_name}} {{$first_name}}</span>,<br>
            Nous avons bien reçu votre candidature au poste de <span class="span_p">{{$job}}</span> et vous remercions
            de l’intérêt
            que vous portez à Devinweb.<br>
            Votre candidature fera l’objet d’une étude approfondie.<br>Nous ne manquerons pas de vous tenir informé(e)
            de la suite qui lui sera donnée dans les meilleurs délais.
            Sans réponse de notre part durant les trois semaines qui suivent votre candidature, considérez que
            celle-ci est négative.<br>
            Nous vous prions d’agréer nos salutations distinguées.
            Équipe de recrutement.<br>
            Cordialement,<br>
            --<br>
            <span class="s">Hasna GHERSELLA</span><br>
            <span class="s">Département RH</span><br>
            <span class="s">Tel: +212667789729</span><br>
            <span class="s">http://devinweb.com</span><br>
        </p>
    </div>
    <img src="http://d2wwknes1cy3mz.cloudfront.net/public/logo-b.png" alt="http://devinweb.com">
</body>

</html>