<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Applied Job</title>

    <style>
        #div-flex {
            display: flex;
            justify-content: center;
        }
        #title{
            margin-top: 15px;
            margin-bottom: 15px;
            font-size: large;
            text-align: center;
            font-weight: bold;
            font-family: Ubuntu;justify-content: center;
            font-family: Ubuntu;
            color: purple;
        }

        #div_1 {
            width: 50%;
            font-weight: bold;
            margin-bottom: 5px;
            padding: 5px;
            font-family: Ubuntu;
            border-bottom: 2px solid purple;
            background-color: #d4cce0;
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
        }

        #div_2 {
            width: 50%;            
            margin-bottom: 5px;
            padding: 5px;
            font-family: Ubuntu;
            border-bottom: 2px solid purple;
            background-color: #d4cce0;
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
        }
    </style>


</head>

<body>
    <div id="title">
        Application for {{$job}}
    </div>
    <div>
        @foreach ($candidate as $key=>$value)
        <div id="div-flex">
            <div id="div_1">{{$key}}</div>
            <div id="div_2">@if (is_array($value))
                @foreach ($value as $v)
                {{$v}},
                @endforeach



                @else
                {{$value}}
                @endif</div>
        </div>
        @endforeach
    </div>
</body>

</html>