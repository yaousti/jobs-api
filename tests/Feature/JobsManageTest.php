<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Artisan;
use Spatie\Permission\Models\Role;
use Tests\TestCase;

class JobsManageTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Artisan::call('migrate:fresh');
        Artisan::call('db:seed');
        Artisan::call('passport:install');
    }

    public function test_spuer_admin_can_create_user_with_role_given()
    {
        $user = User::create([
            'name' => 'adam',
            'email' => 'adam@gmail.com',
            'password' => bcrypt($password = 'password')
        ]);

        $role = Role::create(['name' => 'regular_user']);

        $user->assignRole([$role->id]);


        $response = $this->json(
            'POST',
            '/api/users',
            [
                'name' => 'adam',
                'email' => 'adam@gmail.com',
                'password' => 'password',
                "confirm-password" => "password",
            ]
        );


        $response
            ->assertStatus(201)
            ->assertJson([
                'created' => true,
            ]);
    }
    public function test_super_admin_user_can_login_with_correct_credentials()
    {

        $response = $this->post('/api/login', [
            'email' => 'yaousti@gmail.com',
            'password' => 'P@ssw0rd',
        ]);

        $response->assertStatus(200);
    }

    public function test_regular_user_can_login_with_correct_credentials()
    {
        $user = User::create([
            'name' => 'adam',
            'email' => 'adam@gmail.com',
            'password' => bcrypt($password = 'password')
        ]);

        $role = Role::create(['name' => 'regular_user']);

        $user->assignRole([$role->id]);

        $response = $this->post('/api/login', [
            'email' => $user->email,
            'password' => $password,
        ]);
        $response->assertStatus(200);
    }

    public function test_unauthenticated_users_can_index_all_jobs()
    {
        $response = $this->json('GET', '/api/jobs');
        $response->assertStatus(200);
    }

    public function test_unauthenticated_users_cant_create_job()
    {
        $response = $this->json('POST', '/api/jobs');
        $response->assertStatus(405);
    }

    public function test_unauthenticated_users_cant_update_job()
    {
        $response = $this->json('PUT', '/api/jobs');
        $response->assertStatus(405);
    }

    public function test_unauthenticated_users_cant_delete_job()
    {
        $response = $this->json('DELETE', '/api/jobs');
        $response->assertStatus(405);
    }
}
