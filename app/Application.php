<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $hidden = [
        'created_at',
        'updated_at'
    ];
    
    protected $fillable = [
        'fields',
        'job_id'
    ];

    protected $casts = [
        'fields' => 'array',
    ];

    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
