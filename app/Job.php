<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    
    protected $fillable = [
        'title',
        'image',
        'details',
        'fields'
    ];

    protected $casts = [
        'fields' => 'array',
    ];

    public function applications()
    {
        return $this->hasMany(Application::class);
    }
}
