<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobController extends Controller
{
    public function jobApplications($id)
    {
        if (!auth()->user()->hasRole('super_admin')) {
            return response()->json(
                ['error' => 'User have not permission to read the job applications'],
                403
            );
        }
        $job = Job::find($id);
        return response()->json(
            [
                'applications' => $job->applications,
                'total' => count($job->applications)
            ],
            200
        );
    }

    public function index()
    {
        $jobs = Job::orderBy('created_at', 'DESC')->get();
        return response()->json([
            'jobs' => $jobs,
            'total' => count($jobs)
        ], 200);
    }

    public function show(Job $job)
    {
        return response()->json(
            ['job' => $job],
            200
        );
    }


    public function store(Request $request)
    {
        if (!auth()->user()->hasRole('super_admin')) {
            return response()->json(
                ['error' => 'User have not permission to create a job'],
                403
            );
        }

        $fields = $request->validate([
            'fields.*.type' => 'required|in:file,date,text,textarea,numeric,string,select,radio,checkbox',
            'fields.*.options' => 'required_if:fields.*.type,select|array',
            'fields.*.label' => 'required|string|max:40',
            'fields.*.required' => 'required|boolean',
            'fields.*.placeholder' => 'nullable|string',
        ]);
        $attributes = $request->validate([
            'title' => 'required',
            'image' => 'required',
            'details' => 'required',
        ]);
        $arr = [];
        foreach ($fields['fields'] as $i => $field) {
            $field['key'] = "field{$i}";

            array_push($arr, $field);
        }

        $attributes['fields'] = json_encode($arr);

        $job = Job::create($attributes);

        return response()->json([
            'success' => 'job has been created',
            'job' => $job
        ], 201);
    }


    public function update(Request $request, Job $job)
    {
        if (!auth()->user()->hasRole('super_admin')) {
            return response()->json(
                ['error' => 'User have not permission to update a job'],
                403
            );
        }

        $request->validate([
            'title' => 'required',
            'city' => 'required',
            'country' => 'required',
            'position' => 'required',
            'details' => 'required',
        ]);

        $job->update($request->all());

        return response()->json([
            'success' => 'job has been updated',
            'job' => $job
        ], 201);
    }


    public function destroy(Job $job)
    {
        if (!auth()->user()->hasRole('super_admin')) {
            return response()->json(
                ['error' => 'User have not permission to delete a job'],
                403
            );
        }

        $job->delete();
        return response()->json(
            ['success' => 'job has been deleted'],
            200
        );
    }
}
