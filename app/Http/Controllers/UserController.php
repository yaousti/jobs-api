<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user->with('roles')->first()], 200);
    }

    public function index()
    {
        if (auth()->user()->hasRole('super_admin')) {
            $data = User::orderBy('id', 'ASC')->get();

            return response()->json([
                'users' => $data,
                'total' => count($data)
            ], 200);
        }
        return response()->json(['error' => 'User have not permission to read list of users'], 403);
    }


    public function store(Request $request)
    {
        if (auth()->user()->hasRole('super_admin')) {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'required|same:confirm-password',
                'roles' => 'required|exists:roles,id'
            ]);

            $input = $request->all();
            $input['password'] = Hash::make($input['password']);
            $user = User::create($input);

            $user->assignRole($request->input('roles'));

            return response()->json(['success' => 'user has been created'], 201);
        }
        return response()->json(['error' => 'User have not permission to create a user'], 403);
    }


    public function show(User $user)
    {
        if (auth()->user()->hasRole('super_admin')) {
            return response()->json(['user' => $user], 200);
        }
        return response()->json(['error' => 'User have not permission to read a user'], 403);
    }


    public function update(Request $request, $id)
    {
        if (auth()->user()->hasRole('super_admin')) {

            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,' . $id,
                'password' => 'same:confirm-password',
                'roles' => 'required'
            ]);

            $input = $request->all();
            if (!empty($input['password'])) {
                $input['password'] = Hash::make($input['password']);
            } else {
                $input = array_except($input, array('password'));
            }

            $user = User::find($id);
            $user->update($input);
            DB::table('model_has_roles')->where('model_id', $id)->delete();

            $user->assignRole($request->input('roles'));

            return response()->json([
                'success' => 'user has been updated',
                'user' => $user
            ], 201);
        }
        return response()->json(['error' => 'User have not permission to update a user'], 403);
    }


    public function destroy($id)
    {
        if (auth()->user()->hasRole('super_admin')) {
            User::find($id)->delete();
            return response()->json(['success' => 'user has been deleted'], 200);
        }
        return response()->json(['error' => 'User have not permission to delete a user'], 403);
    }
}
