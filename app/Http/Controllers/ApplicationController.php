<?php

namespace App\Http\Controllers;

use App\Application;
use App\Job;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class ApplicationController extends Controller
{
    public function index()
    {
        if (!auth()->user()->hasRole('super_admin')) {
            return response()->json(
                ['error' => 'User have not permission to index the job applications'],
                403
            );
        }
        $applications = Application::all();

        return response()->json([
            'applications' => $applications,
            'total' => count($applications)
        ], 200);
    }
    public function show(Application $application)
    {
        if (!auth()->user()->hasRole('super_admin')) {
            return response()->json(
                ['error' => 'User have not permission to show the job application'],
                403
            );
        }
        return response()->json([
            'application' => $application
        ], 200);
    }

    public function showResume($id)
    {
        if (!auth()->user()->hasRole('super_admin')) {
            return response()->json(
                ['error' => 'User have not permission to download the applications resume'],
                403
            );
        }
        $application = Application::findOrFail($id);
        foreach ($application['fields'] as $field) {

            if (is_array($field)) {
                continue;
            }
            if (Str::contains($field, ['.pdf', '.docx'])) {
                return response()->download(storage_path('app/public/' . $field));
            } else {
                return response()->json(['error' => 'resume not found !']);
            }
        }
    }

    public function apply(Request $request, $id)
    {
        $job = Job::find($id);
        $rules = [];

        foreach (collect(json_decode($job->fields)) as $key => $value) {

            $Type = $value->type;

            $Required = $value->required;
            $key = $value->key;


            if ($Required)
                $is_require = "Required";
            else
                $is_require = "nullable";


            if ($Type == "text" || $Type == "string" || $Type == "select" || $Type == "radio" || $Type == "textarea") {
                $rules[$key] = $is_require . "|string";
            } elseif ($Type == "checkbox") {
                $rules[$key] = $is_require . "|array";
            } elseif ($Type == "date") {
                $rules[$key] = $is_require . "|date";
            } elseif ($Type == "numeric") {
                $rules[$key] = $is_require . "|numeric";
            } else {
                $rules[$key] = $is_require . "|file|mimes:pdf,doc,docx|max:4000";
            }
        }

        $validator = Validator::make(request()->all(), $rules);


        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json(['error', $errors], 422);
        }

        foreach ($rules as $key => $rule) {
            if ($request->hasFile($key)) {
                $file = $request->file($key);

                $fileName = basename($request->file($key)->getClientOriginalName(), '.' . $request->file($key)->getClientOriginalExtension())
                    . '_'
                    . date('YmdDHs')
                    . '.'
                    . $request->file($key)->getClientOriginalExtension();
                $fileName = str_replace(' ', '_', $fileName);
                $filePath = storage_path('app/public');
                $file->move($filePath, $fileName);
                $attributes[$key] = $fileName;
            } else {
                $attributes[$key] = $request[$key];
            }
        }
        $application = new Application;
        $application->fields = $attributes;
        $application->job_id = $id;

        $application->save();

        $candidate = [];

        $Type = collect(json_decode($job->fields))->where('key', $key)->first()->label;



        $resume = '';
        foreach ($application->fields as $key => $field) {
            $label = collect(json_decode($job->fields))->where('key', $key)->first()->label;
            if ($label != 'Upload CV') {
                $candidate[$label] = $field;
            } else {
                $resume = $field;
            }
        }
        // return view('accuse', [
        //     'first_name' =>  $candidate['First Name'],
        //     'last_name' => $candidate['Last Name'],
        //     'job' => $job->title
        // ]);


        Mail::send('applied', ['candidate' => $candidate, 'job' => $job->title], function ($message) use ($job, $resume) {
            $message->to('yaousti@gmail.com', 'AOUSTI YOUSSEF')->subject('Application for ' . $job->title)
                ->attach(storage_path('app/public/' . $resume));
        });

        Mail::send('accuse', [
            'first_name' =>  $candidate['First Name'],
            'last_name' => $candidate['Last Name'],
            'job' => $job->title
        ], function ($message) use ($candidate) {
            $message->to($candidate['Email'], $candidate['First Name'] . ' ' . $candidate['Last Name'])->subject('Accusé de réception de votre candidature');
        });

        return response()->json(['application', $application], 201);
    }
}
