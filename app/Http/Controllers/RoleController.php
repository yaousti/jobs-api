<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use DB;


class RoleController extends Controller
{

    public function index()
    {
        if (!auth()->user()->hasRole('super_admin')) {
            return response()->json(['error' => 'User have not permission to index roles'], 403);
        }

        $roles = Role::orderBy('id', 'ASC')->get();
        return response()->json([
            'roles' => $roles,
            'total' => count($roles)
        ], 200);
    }


    public function store(Request $request)
    {
        if (!auth()->user()->hasRole('super_admin')) {
            return response()->json(['error' => 'User have not permission to create a role'], 403);
        }

        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            // 'permission' => 'required',
        ]);


        $role = Role::create(['name' => $request->input('name')]);
        // $role->syncPermissions($request->input('permission'));
        return response()->json(['success' => 'role has beed created'], 201);
    }


    public function show(Role $role)
    {
        if (!auth()->user()->hasRole('super_admin')) {
            return response()->json(['error' => 'User have not permission to read a role'], 403);
        }
        // $rolePermissions = Permission::join("role_has_permissions", "role_has_permissions.permission_id", "=", "permissions.id")
        //     ->where("role_has_permissions.role_id", $id)
        //     ->get();

        return response()->json([
            'role' => $role
            // 'rolePermissions' => $rolePermissions
        ], 200);
    }

    public function update(Request $request, Role $role)
    {
        if (!auth()->user()->hasRole('super_admin')) {
            return response()->json(['error' => 'User have not permission to update a role'], 403);
        }

        $this->validate($request, [
            'name' => 'required'
        ]);


        $role->name = $request->input('name');
        $role->save();

        return response()->json(['success' => 'role has been updated'], 200);
    }


    public function destroy(Role $role)
    {
        if (!auth()->user()->hasRole('super_admin')) {
            return response()->json(['error' => 'User have not permission to delete a role'], 403);
        }

        $role->delete();
        return response()->json(['success' => 'role has bees deleted'], 200);
    }
}
