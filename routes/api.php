<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
// Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/login', 'AuthController@login')->name('login');
Route::post('/registre', 'AuthController@register')->name('registre');
Route::resource('jobs', 'JobController')->only(['index', 'show']);

Route::post('jobs/{id}/apply', 'ApplicationController@apply')->name('job-application');



Route::group(['middleware' => ['auth:api']], function () {
    Route::post('logout', 'AuthController@logout')->name('logout');
    Route::get('/details', 'UserController@details')->name('details');
    Route::resource('application', 'ApplicationController')->only('index', 'show');
    Route::get('application/{id}/resume', 'ApplicationController@showResume')->name('show-resume');
    Route::get('jobs/{id}/applications', 'JobController@jobApplications')->name('applications');

    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController');
    Route::resource('jobs', 'JobController')->except(['index', 'show']);
});
